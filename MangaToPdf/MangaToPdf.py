from fpdf import FPDF
import requests
from os import path
import os
from bs4 import BeautifulSoup
from PIL import Image

def dl_images(url_base, manga_name = '', chapter_from = 1, chapter_to = -1):
    end_reached = False
    chapter = chapter_from

    if manga_name == '':
        manga_name = url_base.split('/')[-1]

    while not end_reached:
        chapter_dir = path.join(img_dir, manga_name, format(chapter, chapter_format))
        if not path.exists(chapter_dir):
            os.makedirs(chapter_dir)
    
        page = 1

        # Download images
        while True:
            url = '{}/{}/{}'.format(url_base, chapter, page)
            html = requests.get(url)
            # Did we reach end of this chapter?
            if html.status_code == 404:
                if page == 1:
                    print('End of series reached')
                    end_reached = True
                else:
                    print('End of chapter reached')
                break
            if html.status_code != 200:
                print('Status code {} returned for {}'.format(html.status_code, url))
    
            soup = BeautifulSoup(html.content, 'html.parser')

            image = soup.find(id='img', src=True)
            if image is None:
                print('Image not found in {}'.format(url))
                if page == 1: # Chapter probably missing
                    break
            else:
                link = image['src']
                r = requests.get(link)
            
                ext = link.split('.')[-1]
                # Leading zeroes for simple sort in the directory
                dl_dir = path.join(chapter_dir, format(page, img_format) + '.' + ext)
                print('Downloading {} to {}'.format(link, dl_dir))
                with open(dl_dir, 'wb') as out_file:
                    out_file.write(r.content)
            page += 1

        chapter += 1
        if chapter > chapter_to and chapter_to != -1:
            print('Final chapter downloaded')
            end_reached = True


A4_dimensions = {'w': 210, 'h': 297}

def create_pdf(src_dir, file_name, out_dir = ''):
    print('Generating pdf {} from {}'.format(path.join(out_dir, file_name), src_dir))
    if not path.exists(out_dir) and out_dir != '':
        os.makedirs(out_dir)

    pdf = FPDF()
    for f in sorted(os.listdir(src_dir)):
        pdf.add_page()
        
        img_path = path.join(src_dir, f);

        # Detect landscape images and scale them down
        w = 0
        h = 0
        im = Image.open(img_path)
        if im.width > im.height:
            w = A4_dimensions['w']
        else:
            h = A4_dimensions['h']

        pdf.image(img_path, 0, 0, w, h)
    pdf.output(path.join(out_dir, file_name + '.pdf'), 'F')


def generate_pdfs(src_dir, name, chapter = -1):
    dir_img = path.join(src_dir, name)
    if chapter != -1:
        chapter_path = path.join(dir_img, format(chapter, chapter_format))
        create_pdf(chapter_path, name + '_' + d, path.join(pdf_dir, name))
    else:
        for d in os.listdir(dir_img):
            chapter_path = path.join(dir_img, d)
            create_pdf(chapter_path, name + '_' + d, path.join(pdf_dir, name))



# Main
#url_base = 'http://www.mangareader.net/berserk'
#url_base = 'http://www.mangareader.net/last-game'
url_base = 'http://www.mangareader.net/special-a'
img_dir = 'images'
pdf_dir = 'pdfs'
chapter_format = '03d'
img_format = '05d'

dl_images(url_base, chapter_from = 1, chapter_to = -1)
generate_pdfs(img_dir, 'last-game')
